package config;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

public class DataConfig {

    private Properties prop = null;

    public DataConfig() {
        InputStream inputStream = DataConfig.class.getClassLoader().getResourceAsStream("data.properties");
        try {
            prop = new Properties();
            prop.load(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getBaseUrl() {
        return prop.getProperty("baseUrl");
    }

    public String getFirstName(){
        return prop.getProperty("firstName");
    }

    public String getLastName(){
        return prop.getProperty("lastName");
    }
    public String getFullName(){
        return getFirstName() + " " + getLastName();
    }
    public String getExistingFullName(){
        return prop.getProperty("existingFirstName") + " " + prop.getProperty("existingLastName");
    }

    public String getMobileNumber(){
        return prop.getProperty("mobileNumber");
    }

    public String getRandomString(){
        Random rand = new Random();
        String characters = "abcdefghijklmnoprstquvwxyz0123456789";
        String randomString = "";
        int randomStringSize = characters.length();
        for(int i = 0 ; i<10;i++){
            randomString = randomString + characters.charAt(rand.nextInt(randomStringSize));
        }
        return randomString;
    }

    public String getRegistrationValidMail(){
        String registrationValidMail =  prop.getProperty("validMail");
        registrationValidMail = registrationValidMail.replace("<RANDOM_TEXT>",getRandomString());
        return registrationValidMail;
    }

    public String getValidPassword(){
        return prop.getProperty("validPassword");
    }
    public String getInvalidPassword(){
        return prop.getProperty("invalidPassword");
    }

    public String getInvalidMail(){
        return prop.getProperty("invalidMail");
    }
    public String getEmail(){
        return prop.getProperty("email");
    }

    public String getPassword(){
        return prop.getProperty("password");
    }

    public String getDestination(){
        return prop.getProperty("destination");
    }

    public String getCurrentDate(){
        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        return date;
    }
}
