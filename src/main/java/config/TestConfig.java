package config;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class TestConfig {

    private WebDriver driver = null;
    private DataConfig data = new DataConfig();
    private DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

    /*Creating WebDriver instance depends of parameter from xml suite file*/
    public WebDriver createDriver(String browser, String type) throws MalformedURLException {
        if(browser.equals("chrome") && type.equals("remote")){
            desiredCapabilities = DesiredCapabilities.chrome();
            desiredCapabilities.setPlatform(Platform.LINUX);
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), desiredCapabilities);
        }
        else if(browser.equals("firefox") && type.equals("remote")){
            desiredCapabilities = DesiredCapabilities.firefox();
            desiredCapabilities.setPlatform(Platform.LINUX);
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), desiredCapabilities);
        }
        else if (browser.equals("chrome")&& type.equals("desktop")){
            driver = new ChromeDriver();
        }else if (browser.equals("firefox")&& type.equals("desktop")){
            driver = new FirefoxDriver();
        }
        return driver;
    }

    /*Test SetUp method*/
    public void loadHomePage(){
        String baseUrl =  data.getBaseUrl();
        driver.get(baseUrl);
    }

}
