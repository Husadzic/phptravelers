package page.objects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class HomePage extends Page{


    @FindBy (xpath="//form[@name='HOTELS']//span[contains(text(),'Search by Hotel or City Name')]/parent::a")
    WebElement destinationBox;

    @FindBy (xpath="//div[@id='select2-drop']//input")
    WebElement destionationInput;

    @FindBy (xpath = "//span[contains(@class,'select2-match')]")
    List<WebElement> destinationResults;

    @FindBy (id="checkin")
    WebElement checkInBox;

    @FindBy (id="checkout")
    WebElement checkOutBox;

    @FindBy (xpath="//form[@name=\"HOTELS\"]//button[@type='submit']")
    WebElement searchButton;

    @FindBy (id="listing")
    WebElement resultsListing;

    public HomePage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public boolean isLogoVisible(){
        return headerSection.isHeaderLogoDisplayed();
    }

    public void insertDestination(String destination){
        wait.until(ExpectedConditions.visibilityOf(destinationBox));
        destinationBox.click();
        destionationInput.sendKeys(destination);
        wait.until(ExpectedConditions.visibilityOfAllElements(destinationResults));
        for(WebElement destinationResult : destinationResults){
            if(destinationResult.getText().equals(destination)) {
                destinationResult.click();
            }
        }
    }
    public void insertCheckIn(String checkIn){
        checkInBox.sendKeys(checkIn);
        checkInBox.sendKeys(Keys.TAB);
    }
    public void insertCheckOut(String checkOut){
        checkOutBox.sendKeys(checkOut);;
        checkOutBox.sendKeys(Keys.TAB);
    }
    public void clickSearch(){
        searchButton.click();
    }

    public boolean isListingApear(){
        wait.until(ExpectedConditions.visibilityOf(resultsListing));
        return resultsListing.isDisplayed();
    }

}
