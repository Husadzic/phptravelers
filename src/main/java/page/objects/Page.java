package page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.objects.page.sections.FooterSection;
import page.objects.page.sections.HeaderSection;

public class Page {
    protected HeaderSection headerSection;
    protected FooterSection footerSection;
    protected WebDriverWait wait;

    public Page(WebDriver driver) {
        wait = new WebDriverWait(driver, 10,50);
        this.headerSection = new HeaderSection(driver, wait);
        this.footerSection = new FooterSection(driver, wait);
    }

    public HeaderSection getHeaderSection() {
        return headerSection;
    }

    public boolean isPageLoaded(){
        boolean isLoadCompleted = false;
        if(wait.until(ExpectedConditions.jsReturnsValue("return document.readyState=='complete';"))!=null){
            if(headerSection.isHeaderVisible()){
                if(footerSection.isFooterVisible()) {
                    isLoadCompleted = true;
                }
            }
        }
        return isLoadCompleted;
    }

    public String getPageTitle(WebDriver driver){
        return driver.getTitle();
    }

}
