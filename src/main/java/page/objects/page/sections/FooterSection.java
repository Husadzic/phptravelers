package page.objects.page.sections;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FooterSection {
    protected WebDriverWait wait;

    @FindBy (id="footer")
    WebElement footer;

    public FooterSection(WebDriver driver, WebDriverWait wait){
        PageFactory.initElements(driver,this);
        this.wait = wait;
    }

    public boolean isFooterVisible(){
        wait.until(ExpectedConditions.visibilityOf(footer));
        return footer.isDisplayed();
    }
}
