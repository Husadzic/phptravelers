package page.objects.page.sections;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HeaderSection {
    protected WebDriverWait wait;

    @FindBy (id="header-waypoint-sticky")
    WebElement topHeader;

    @FindBy (xpath="//div[contains(@class, 'dropdown')]//i[contains(@class,'bx')]")
    WebElement myAccount;

    @FindBy (xpath="//div[contains(@class, 'dropdown-menu-right')]//a[text()='Sign Up']")
    WebElement signUp;

    @FindBy (xpath="//div[contains(@class, 'dropdown-menu-right')]//a[text()='Login']")
    WebElement login;

    @FindBy (xpath="//div[contains(@class, 'header-logo')]")
    WebElement headerLogo;

    @FindBy (xpath = "//i[contains(@class, 'bx bx-user')]/parent::a")
    WebElement userName;

    public HeaderSection(WebDriver driver, WebDriverWait wait){
        PageFactory.initElements(driver, this);
        this.wait = wait;
    }


    public void navigateToSignUp(){
        wait.until(ExpectedConditions.elementToBeClickable(myAccount));
        myAccount.click();
        signUp.click();
    }
    public void navigateToLogin(){
        wait.until(ExpectedConditions.elementToBeClickable(myAccount));
        myAccount.click();
        login.click();
    }
    public boolean isHeaderVisible(){
        wait.until(ExpectedConditions.visibilityOf(topHeader));
        return topHeader.isDisplayed();
    }
    public boolean isHeaderLogoDisplayed(){
        wait.until(ExpectedConditions.visibilityOf(headerLogo));
        return headerLogo.isDisplayed();
    }

    public String getUserName(){
        wait.until(ExpectedConditions.visibilityOf(userName));
        return userName.getText().trim().toLowerCase();
    }
}
