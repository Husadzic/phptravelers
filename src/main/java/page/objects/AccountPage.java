package page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AccountPage extends Page {

    @FindBy (xpath="//h3[contains(text(),'Hi,')]")
    static WebElement userData;

    public AccountPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getUserData(){
        wait.until(ExpectedConditions.visibilityOf(userData));
        String fullName = userData.getText().replace("Hi, ", "");
        return fullName;
    }

}
