package page.objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginPage extends Page {

    @FindBy (xpath="//input[@name='username']")
    WebElement emailTextBox;

    @FindBy (xpath = "//input[@name='password']")
    WebElement passwordTextBox;

    @FindBy (xpath = "//button[contains(@class, 'loginbtn')]")
    WebElement loginButton;

    @FindBy (xpath = "//form[@id='loginfrm']//a[contains(text(),'Login')]")
    WebElement loginTitle;
    @FindBy (xpath = "//div[contains(@class, 'alert-danger')]")
    WebElement messageError;
    @FindBy (xpath="//label[@for='remember-me']")
    WebElement rememberMe;

    public LoginPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public void insertEmail(String email){
        wait.until(ExpectedConditions.visibilityOf(emailTextBox));
        emailTextBox.sendKeys(email);
    }
    public void insertPassword(String password){
        passwordTextBox.sendKeys(password);
    }

    public void clickLoginButton(){
        wait.until(ExpectedConditions.elementToBeClickable(loginButton));
        loginButton.click();
    }

    public String getLoginTitle(){
        wait.until(ExpectedConditions.visibilityOf(loginTitle));
        return loginTitle.getText().trim();
    }

    public boolean isErrorApear(){
        wait.until(ExpectedConditions.visibilityOf(messageError));
        return messageError.isDisplayed();
    }

    public void clickRememberMe(){
        wait.until(ExpectedConditions.elementToBeClickable(rememberMe));
        rememberMe.click();
    }

}
