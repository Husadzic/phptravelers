package page.objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SignUpPage extends Page {

    static final Logger logger = LogManager.getLogger(SignUpPage.class.getName());
    @FindBy (xpath ="//div[@class='container']//a[contains(text(),'Register')]")
    WebElement registerTitle;

    @FindBy (xpath = "//input[@name='firstname']")
    WebElement firstNameTextBox;

    @FindBy (xpath="//input[@name='email']")
    WebElement emailTextBox;

    @FindBy(xpath = "//input[@name='lastname']")
    WebElement lastNameTextBox;

    @FindBy (xpath = "//input[@name='phone']")
    WebElement phoneTextBox;

    @FindBy (xpath = "//input[@name='password']")
    WebElement passwordTextBox;

    @FindBy (xpath = "//input[@name='confirmpassword']")
    WebElement passwordConfirmTextBox;

    @FindBy (xpath = "//button[contains(@type,'submit') and contains(@class,'signupbtn')]")
    WebElement signUpButton;

    @FindBy (xpath = "//div[contains(@class, 'alert-danger')]")
    WebElement messageError;

    public SignUpPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public String getRegisterTitle(){
        wait.until(ExpectedConditions.visibilityOf(registerTitle));
        return registerTitle.getText().trim();
    }

    public void insertFirstName(String firstName){
        wait.until(ExpectedConditions.visibilityOf(firstNameTextBox));
        firstNameTextBox.sendKeys(firstName);
    }

    public void insertLastName(String lastName){
        lastNameTextBox.sendKeys(lastName);
    }

    public void insertMobileNumber(String phone){
        phoneTextBox.sendKeys(phone);
    }

    public void insertEmail(String email){
        emailTextBox.sendKeys(email);
    }

    public void insertPassword(String password){
        passwordTextBox.sendKeys(password);
    }
    public void insertPasswordConfirmation(String passwordConfirm){
        passwordConfirmTextBox.sendKeys(passwordConfirm);
    }

    public void clickSignUpBtn(){
        signUpButton.click();
    }
    public boolean isErrorApear(){
        wait.until(ExpectedConditions.visibilityOf(messageError));
        return messageError.isDisplayed();
    }
}
