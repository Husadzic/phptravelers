import config.DataConfig;
import config.TestConfig;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import page.objects.HomePage;

import java.net.MalformedURLException;

public class HomePageTest {
    private WebDriver driver;
    private HomePage homePage;
    private TestConfig testConfig = new TestConfig(); //Config test set up, choose browser, open base URL and start the test.
    private DataConfig data = new DataConfig();



    @BeforeClass
    @Parameters ({"browser", "type"})
    public void setUp(String browser, String type) throws MalformedURLException {
        driver = testConfig.createDriver(browser, type); //return instance of webdriver, i.e (chrome or firefox)
        homePage = new HomePage(driver);
    }

    @BeforeMethod
    public void loadHome(){
        testConfig.loadHomePage(); //Starting test set up
        Assert.assertTrue(homePage.isPageLoaded());
    }

    @Test
    public void homePage_CheckHeaderLogo_shouldBeVisible(){
        Assert.assertTrue(homePage.isLogoVisible());
    }

    @Test
    public void searchHotels_DestinationNewYork_HotelListsShouldApear(){
        homePage.insertDestination(data.getDestination());
        homePage.insertCheckIn(data.getCurrentDate());
        homePage.insertCheckOut(data.getCurrentDate());
        homePage.clickSearch();
        Assert.assertTrue(homePage.isListingApear());
    }
    @Test
    public void searchHotels_DestinationNewYork_PageTitleShouldBeCorrect(){
        homePage.insertDestination(data.getDestination());
        homePage.insertCheckIn(data.getCurrentDate());
        homePage.insertCheckOut(data.getCurrentDate());
        homePage.clickSearch();
        Assert.assertEquals(homePage.getPageTitle(driver), "Hotels Results");
    }

    @AfterMethod
    public void clearBrowserCache(){
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }


}
