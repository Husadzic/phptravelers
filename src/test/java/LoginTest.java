import config.DataConfig;
import config.TestConfig;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import page.objects.AccountPage;
import page.objects.HomePage;
import page.objects.LoginPage;

import java.net.MalformedURLException;

public class LoginTest {
    private WebDriver driver;
    private TestConfig testConfig = new TestConfig();
    private DataConfig data = new DataConfig();  //Read data from properties
    private HomePage homePage;
    private LoginPage loginPage;
    private AccountPage accountPage;

    @BeforeClass
    @Parameters({"browser", "type"})
    public void setUp(String browser, String type) throws MalformedURLException {
        driver = testConfig.createDriver(browser, type);
        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
        accountPage = new AccountPage(driver);
    }

    @BeforeMethod
    public void loadHome(){
        testConfig.loadHomePage();
        Assert.assertTrue(homePage.isPageLoaded());
    }

    @Test
    public void loginUser_DataIsValid_ShouldOpenProfilePage(){
        homePage.getHeaderSection().navigateToLogin();
        Assert.assertEquals(loginPage.getLoginTitle(), "Login");
        loginPage.insertEmail(data.getEmail());
        loginPage.insertPassword(data.getPassword());
        loginPage.clickLoginButton();
        Assert.assertEquals(accountPage.getUserData(), data.getExistingFullName());
    }
    @Test
    public void loginUser_LeaveEmailEmpty_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToLogin();
        Assert.assertEquals(loginPage.getLoginTitle(), "Login");
        loginPage.insertEmail("");
        loginPage.insertPassword(data.getPassword());
        loginPage.clickLoginButton();
        Assert.assertTrue(loginPage.isErrorApear());
    }
    @Test
    public void loginUser_LeavePasswordEmpty_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToLogin();
        Assert.assertEquals(loginPage.getLoginTitle(), "Login");
        loginPage.insertEmail(data.getEmail());
        loginPage.insertPassword("");
        loginPage.clickLoginButton();
        Assert.assertTrue(loginPage.isErrorApear());
    }
    @Test
    public void loginUser_LeaveAllFieldsEmpty_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToLogin();
        Assert.assertEquals(loginPage.getLoginTitle(), "Login");
        loginPage.insertEmail("");
        loginPage.insertPassword("");
        loginPage.clickLoginButton();
        Assert.assertTrue(loginPage.isErrorApear());
    }
    @Test
    public void loginUser_TryLoginWithInvalidPassword_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToLogin();
        Assert.assertEquals(loginPage.getLoginTitle(), "Login");
        loginPage.insertEmail(data.getEmail());
        loginPage.insertPassword(data.getInvalidPassword());
        loginPage.clickLoginButton();
        Assert.assertTrue(loginPage.isErrorApear());
    }
    @Test
    public void loginUser_TryLoginWithInvalidEmail_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToLogin();
        Assert.assertEquals(loginPage.getLoginTitle(), "Login");
        loginPage.insertEmail(data.getInvalidMail());
        loginPage.insertPassword(data.getPassword());
        loginPage.clickLoginButton();
        Assert.assertTrue(loginPage.isErrorApear());
    }

    @AfterMethod
    public void clearBrowserCache(){
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }


}
