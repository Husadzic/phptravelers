import config.DataConfig;
import config.TestConfig;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import page.objects.AccountPage;
import page.objects.HomePage;
import page.objects.SignUpPage;

import java.net.MalformedURLException;

public class RegistrationTest {
    private WebDriver driver;
    private HomePage homePage;
    private SignUpPage signUpPage;
    private AccountPage accountPage;
    private TestConfig testConfig = new TestConfig();
    private DataConfig data = new DataConfig();

    @BeforeClass
    @Parameters({"browser", "type"})
    public void setUp(String browser, String type) throws MalformedURLException {
        driver = testConfig.createDriver(browser, type);
        homePage = new HomePage(driver);
        signUpPage = new SignUpPage(driver);
        accountPage = new AccountPage(driver);
    }

    @BeforeMethod
    public void loadHome(){
        testConfig.loadHomePage();
        Assert.assertTrue(homePage.isPageLoaded());
    }
    @Test
    public void registerNewUser_UserDataIsValid_ShouldOpenProfilePage(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName(data.getFirstName());
        signUpPage.insertLastName(data.getLastName());
        signUpPage.insertMobileNumber(data.getMobileNumber());
        signUpPage.insertEmail(data.getRegistrationValidMail());
        signUpPage.insertPassword(data.getValidPassword());
        signUpPage.insertPasswordConfirmation(data.getValidPassword());
        signUpPage.clickSignUpBtn();
        Assert.assertEquals(accountPage.getUserData(), data.getFullName());
    }
    @Test
    public void registerNewUser_UserDataIsValidLeavePhoneEmpty_ShouldOpenProfilePage(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName(data.getFirstName());
        signUpPage.insertLastName(data.getLastName());
        signUpPage.insertMobileNumber("");
        signUpPage.insertEmail(data.getRegistrationValidMail());
        signUpPage.insertPassword(data.getValidPassword());
        signUpPage.insertPasswordConfirmation(data.getValidPassword());
        signUpPage.clickSignUpBtn();
        Assert.assertEquals(accountPage.getUserData(), data.getFullName());
    }
    @Test
    public void registerNewUser_EmailNotValid_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName(data.getFirstName());
        signUpPage.insertLastName(data.getLastName());
        signUpPage.insertMobileNumber(data.getMobileNumber());
        signUpPage.insertEmail(data.getInvalidMail());
        signUpPage.insertPassword(data.getValidPassword());
        signUpPage.insertPasswordConfirmation(data.getValidPassword());
        signUpPage.clickSignUpBtn();
        Assert.assertTrue(signUpPage.isErrorApear());
    }
    @Test
    public void registerNewUser_UseAlreadyExistingEmail_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName(data.getFirstName());
        signUpPage.insertLastName(data.getLastName());
        signUpPage.insertMobileNumber(data.getMobileNumber());
        signUpPage.insertEmail(data.getEmail());  //Email of existing user
        signUpPage.insertPassword(data.getValidPassword());
        signUpPage.insertPasswordConfirmation(data.getValidPassword());
        signUpPage.clickSignUpBtn();
        Assert.assertTrue(signUpPage.isErrorApear());
    }

    @Test
    public void registerNewUser_LeaveMandatoryFieldsEmpty_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName("");
        signUpPage.insertLastName("");
        signUpPage.insertMobileNumber(data.getMobileNumber());
        signUpPage.insertEmail("");
        signUpPage.insertPassword("");
        signUpPage.insertPasswordConfirmation("");
        signUpPage.clickSignUpBtn();
        Assert.assertTrue(signUpPage.isErrorApear());
    }
    @Test
    // Invalid password should be less then 6 characters long
    public void registerNewUser_UseInvalidPassword_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName(data.getFirstName());
        signUpPage.insertLastName(data.getLastName());
        signUpPage.insertMobileNumber(data.getMobileNumber());
        signUpPage.insertEmail(data.getRegistrationValidMail());
        signUpPage.insertPassword(data.getInvalidPassword());
        signUpPage.insertPasswordConfirmation(data.getInvalidPassword());
        signUpPage.clickSignUpBtn();
        Assert.assertTrue(signUpPage.isErrorApear());
    }
    @Test
    public void registerNewUser_PasswordNotMatchWithConfirmPassword_ErrorMessageShouldApear(){
        homePage.getHeaderSection().navigateToSignUp();
        Assert.assertEquals(signUpPage.getRegisterTitle(), "Register");
        signUpPage.insertFirstName(data.getFirstName());
        signUpPage.insertLastName(data.getLastName());
        signUpPage.insertMobileNumber(data.getMobileNumber());
        signUpPage.insertEmail(data.getRegistrationValidMail());
        signUpPage.insertPassword(data.getInvalidPassword());
        signUpPage.insertPasswordConfirmation(data.getValidPassword());
        signUpPage.clickSignUpBtn();
        Assert.assertTrue(signUpPage.isErrorApear());
    }

    @AfterMethod
    public void clearBrowserCache(){
        driver.manage().deleteAllCookies();
    }

    @AfterClass
    public void tearDown(){
        driver.quit();
    }

}

